package micromais.com.br.mmlaudos2.Classes;

import android.os.AsyncTask;

import java.sql.ResultSet;
import java.sql.Connection;

/**
 * Created by Dan on 30/12/2016.
 */

public class ExecuteBD extends AsyncTask<String,Void,ResultSet> {

    private Connection connection;
    private String query;

    public ExecuteBD(Connection connection, String query) {
        this.connection = connection;
        this.query = query;
    }

    @Override
    protected ResultSet doInBackground(String... params) {
        ResultSet resultSet = null;
        try{
            resultSet = connection.prepareStatement(query).executeQuery();
        }catch (Exception e){

        }finally {
            try {
                connection.close();
            }catch (Exception ex){

            }
        }
        return resultSet;
    }
    @Override
    protected void onPostExecute (ResultSet resultSet){
        if (resultSet==null){
            //TODO
        }
    }
}