package micromais.com.br.mmlaudos2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import micromais.com.br.mmlaudos2.Classes.Paciente;
import micromais.com.br.mmlaudos2.Classes.PacienteAdapter;

public class MainActivity extends AppCompatActivity {

    public static final String TAG_SALVAR_GERAL = "Config";   // tag que salva informaçoes de configuraçao
    public static final String TAG_IP = "IpValor";            // tag que salva ip
    public static final String TAG_LOGIN = "loginvalor";      // tag que salva lodin ususario
    public static final String TAG_SENHA = "senhavalor";      // tag que salva senha usuario
    public static final String TAG_MEDICO = "valormedico";
    public static final String TAG_NOME = "valornome";

    public static String IpServer = null;  //ip servidor web
    public static String Usuario;
    public static String Senha;
    public static int MedCodigo;
    public static String Nome;

    private ProgressDialog dialog;
    public boolean telaInicial;
    private int backButtonCount = 0;
    Paciente pac = new Paciente();
    boolean conexaoSuces;

    private ListView listar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferencias = getSharedPreferences(TAG_SALVAR_GERAL, MODE_PRIVATE);
        telaInicial = preferencias.getBoolean(getString(R.string.iniciar_tela), false);
        IpServer = preferencias.getString(TAG_IP, null);
        Usuario = preferencias.getString(TAG_LOGIN, "");
        Senha = preferencias.getString(TAG_SENHA, "");
        Nome = preferencias.getString(TAG_NOME,"");
        MedCodigo = preferencias.getInt(TAG_MEDICO, 0);

        if (!telaInicial) {
            Intent intConf = new Intent();
            intConf.setClass(getBaseContext(), ConfigActivity.class);
            startActivity(intConf);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView tilutoToolBar = (TextView) findViewById(R.id.toolbarTitlo);
        tilutoToolBar.setText("MMLaudos");
        TextView nomeToolBar = (TextView) findViewById(R.id.toolbarTV);
        nomeToolBar.setText(Nome);

        pac.getLista();

        new EfetuandoTeste().execute("SUPERVISOR");

        //PacienteAdapter adaptador = new PacienteAdapter();
        this.listar = (ListView) findViewById(R.id.lista);
        listar.setAdapter(new PacienteAdapter(getApplicationContext(), pac.listaPaciente()));

        /*final EditText buscar = (EditText) findViewById(R.id.buscaET);
        buscar.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = buscar.getText().toString().toLowerCase(Locale.getDefault());
                (new PacienteAdapter(getApplicationContext(), new Paciente().getLista())).filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });*/

    }

    //criação do menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    //itens do menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intConf = new Intent();
            intConf.setClass(getBaseContext(), ConfigActivity.class);
            startActivity(intConf);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //sair do app
    @Override
    public void onBackPressed() {
        if (backButtonCount >= 1) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Pressione novamente para sair", Toast.LENGTH_SHORT).show();
            backButtonCount++;
        }
    }

    class EfetuandoTeste extends AsyncTask<String, String, Boolean> {

        //executa antes, em caso de demora na resposta do servidor
        @Override
        protected void onPreExecute() {

            // Cria a caixa de dialogo em quanto faz a conexão
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setMessage("Atualizando...");
            dialog.show();
            super.onPreExecute();
        }

        //metotod que realiza a ação
        protected Boolean doInBackground(String... params) {

            if (pac.is_status()) {
                conexaoSuces = true;

            } else {
                conexaoSuces = false;
            }

            return conexaoSuces;
        }

        //apos a consulta
        @Override
        protected void onPostExecute(Boolean conexao) {

            if (conexao) {
                dialog.dismiss();
                TextView nomeToolBar = (TextView) findViewById(R.id.toolbarTV);
                nomeToolBar.setText(Nome);
                Toast.makeText(getApplicationContext(), "Total de Pacientes: "+ listar.getCount(), Toast.LENGTH_LONG).show();

            } else {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Erro de conexão \n Verifique os dados em Configurações", Toast.LENGTH_LONG).show();

            }
            super.onPostExecute(conexao);
        }
    }


}