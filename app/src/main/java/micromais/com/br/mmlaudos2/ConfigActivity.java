package micromais.com.br.mmlaudos2;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static android.R.id.progress;

public class ConfigActivity extends AppCompatActivity {

    EditText ipUrl;
    EditText usuario;
    EditText senha;

    private Connection conn = null;
    private Statement st;
    private ResultSet rs;

    public static String ipValor = null;
    private String loginValor;
    private String senhaValor;
    private String mensagem;
    private String primeiroNome = null;
    private int codMed;
    boolean loginSucess;
    boolean ipSucesso = false;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_voltar));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("cek", "home selected");
                onBackPressed();
            }
        });

        ipUrl = (EditText) findViewById(R.id.ip_config);
        usuario = (EditText) findViewById(R.id.t_usuario);
        senha = (EditText) findViewById(R.id.t_senha);

        SharedPreferences sp1 = getSharedPreferences(MainActivity.TAG_SALVAR_GERAL, MODE_PRIVATE);
        ipValor = sp1.getString(MainActivity.TAG_IP, "");
        ipUrl.setText(ipValor);
        loginValor = sp1.getString(MainActivity.TAG_LOGIN, "");
        usuario.setText(loginValor);
        senhaValor = sp1.getString(MainActivity.TAG_SENHA, "");
        senha.setText(senhaValor);

        Button continuar = (Button) findViewById(R.id.confirm);
        continuar.setOnClickListener(clique);
    }

    View.OnClickListener clique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.confirm:
                    if(usuario.getText().toString().isEmpty()||senha.getText().toString().isEmpty()||ipUrl.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(),"Campos Vazios", Toast.LENGTH_SHORT).show();
                    }else {
                        ipValor = ipUrl.getText().toString();
                        loginValor = usuario.getText().toString();
                        senhaValor = senha.getText().toString();

                        new conectaBanco().execute("SUPERVISOR");
                    }
                    break;
            }
        }
    };

    class conectaBanco extends AsyncTask<String, String, Boolean> {

        //executa antes, em caso de demora na resposta do servidor
        @Override
        protected void onPreExecute() {
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
            super.onPreExecute();
        }

        //metotod que realiza a ação em outra tread
        protected Boolean doInBackground(String... params) {

            if (validaIp(ipValor)) {
                ipSucesso = true;
                conectarPostgres();
                if (loginSucess) {
                    loginUser(loginValor, senhaValor);
                    desconectarPostgres();
                    publishProgress("" + progress);
                } else {
                    mensagem="IP Incorreto, não é possível conectar!";
                    Log.e("Logando", "erro login backgorund");
                }

            } else {
                mensagem="IP Inválido!";
                ipSucesso = false;             //Ip inválido
            }
            return loginSucess;
        }

        protected void onProgressUpdate(String... progress) {
            mProgressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        //apos a consulta
        @Override
        protected void onPostExecute(Boolean loginSucesso) {

            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
            if (ipSucesso&&loginSucesso) {
                SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.TAG_SALVAR_GERAL, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(MainActivity.TAG_IP, ipValor);
                editor.putString(MainActivity.TAG_LOGIN, loginValor);
                editor.putString(MainActivity.TAG_SENHA, senhaValor);
                editor.putInt(MainActivity.TAG_MEDICO,codMed);
                editor.putString(MainActivity.TAG_NOME,primeiroNome);
                editor.putBoolean(getString(R.string.iniciar_tela), Boolean.TRUE);
                editor.apply();

                onBackPressed();
                Toast.makeText(getApplicationContext(), "Dados salvos com sucesso", Toast.LENGTH_SHORT).show();
            } else {
                SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.TAG_SALVAR_GERAL, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(getString(R.string.iniciar_tela), Boolean.FALSE);
                editor.apply();
                Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_SHORT).show();
            }

            Log.e("Banco", "PostExecute");
            super.onPostExecute(loginSucesso);
        }
    }

    //botao voltar
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    public boolean validaIp(String ip) {
        boolean result = true;
        try {
            //separar por partes ip
            ip = ip.replace(".", "#");//troca ponto por # para poder separar, pq ponto é expressao regular
            String[] parts = ip.split("#");

            //valida tamnaho
            if (parts.length != 4)
                result = false;

            //valida cada parte
            for (String p : parts) {
                if (Integer.valueOf(p) < 0 || Integer.valueOf(p) > 255)
                    result = false;
            }

            return result;
        } catch (Exception e) {

            return false;
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Aguarde..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    public boolean conectarPostgres(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            Log.e("Postgres","Erro registrar o driver: "+e);
            e.printStackTrace();
        }
        try {
            DriverManager.setLoginTimeout(5);
            conn = DriverManager.getConnection("jdbc:postgresql://"+ipValor+":5432/pacsdb","postgres","123456");
            Log.e("Postgres","Conectou: ");
            loginSucess=true;
        } catch (SQLException e) {
            loginSucess=false;
            Log.e("Postgres","Erro conexao: "+e+ipValor);
            e.printStackTrace();
        }
        return loginSucess;
    }
    public boolean loginUser (String login, String senha){
        try {
            String sql = "SELECT * FROM mm_medico WHERE login = '" +login+"' and senha = '"+senha+"'";
            st = conn.createStatement();
            rs=st.executeQuery(sql);
            if (rs.next()){
                codMed = rs.getInt("cod_medico");

                String sql2 = "SELECT nome FROM mm_medico WHERE cod_medico='" + codMed + "' ";
                st = conn.createStatement();
                rs=st.executeQuery(sql2);
                while (rs.next()) {
                    String nomeTemp = rs.getString("nome");
                    if (nomeTemp.contains(" ")){
                        primeiroNome = nomeTemp.substring(0, nomeTemp.indexOf(" "));
                    }
                }

                loginSucess=true;
                Log.e("Logando","Conectou");
            }
            else {
                loginSucess=false;
                mensagem="Usuário ou senha incorretos";
                Log.e("Logando","Não Conectou");
            }

        }catch (Exception erro){
            loginSucess=false;
            Log.e("Postgres","Erro de login:"+erro);
        }
        return loginSucess;
    }
    public void desconectarPostgres() {
        try {
            conn.close();
        } catch (SQLException e) {
            Log.e("Postgres","Fechar conexao"+e);
            e.printStackTrace();
        }
    }
}


