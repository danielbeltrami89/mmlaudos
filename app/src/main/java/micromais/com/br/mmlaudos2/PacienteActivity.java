package micromais.com.br.mmlaudos2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.ResultSet;

import micromais.com.br.mmlaudos2.Classes.BancoConexao;

import static micromais.com.br.mmlaudos2.MainActivity.Nome;

public class PacienteActivity extends AppCompatActivity {

    public String pacientes, historias, observacoes, convenios;
    public int laudos, tipos, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView nomeToolBar = (TextView) findViewById(R.id.toolbarTV);
        nomeToolBar.setText(Nome);
        TextView tilutoToolBar = (TextView) findViewById(R.id.toolbarTitlo);
        tilutoToolBar.setText("MMLaudos");

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_voltar));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("cek", "home selected");
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        id = intent.getExtras().getInt("id");

        sqlPaciente2(id);

        TextView pacienteTV = (TextView) findViewById(R.id.pacienti);
        pacienteTV.setText(pacientes);
        TextView hist = (TextView) findViewById(R.id.historicoTV);
        hist.setText(historias);
        TextView obs = (TextView) findViewById(R.id.observacaoTV);
        obs.setText(observacoes);
        TextView conv = (TextView) findViewById(R.id.conveioTV);
        conv.setText(convenios);

        Log.d("ID", "id" + id);

        Button visto = (Button) findViewById(R.id.viulaudo);
        visto.setOnClickListener(clique);
    }

    View.OnClickListener clique = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.viulaudo:
                    vistoMed();
                    onBackPressed();
                    break;
            }
        }
    };


    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void vistoMed() {
        String comando = "";
        comando = String.format("UPDATE mm_paciente SET envia_medico = 1 WHERE id=" + id + ";");
        BancoConexao cb = new BancoConexao();
        cb.execute(comando);

    }

    public void sqlPaciente2 (int idPaciente){
        BancoConexao bd = new BancoConexao();

        try{
            ResultSet rs = bd.select("SELECT historia_cliente, observacao, cod_convenio, situacao_laudo, nome_paciente FROM mm_paciente WHERE id='" + idPaciente + "' ");
            if (rs != null){
                while (rs.next()){
                    historias = rs.getString("historia_cliente");
                    observacoes = rs.getString("observacao");
                    tipos = rs.getInt("cod_convenio");
                    laudos = rs.getInt("situacao_laudo");
                    String pacienteTemp = rs.getString("nome_paciente");
                    pacientes = pacienteTemp.replace("^"," ");

                    rs = bd.select("SELECT descricao FROM mm_convenio WHERE cod_convenio='" + tipos + "' ");
                    if (rs != null) {
                        while (rs.next()) {
                            convenios = rs.getString("descricao");
                        }
                    }
                    Log.e("Postgres", "Consultou historico2");
                }
            }
        }catch (Exception erro){
            Log.e("Postgres", " Não Consultou historico2"+erro);
        }
    }
}
