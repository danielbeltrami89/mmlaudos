package micromais.com.br.mmlaudos2.Classes;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import micromais.com.br.mmlaudos2.PacienteActivity;
import micromais.com.br.mmlaudos2.R;

import static android.R.id.list;

/**
 * Created by Dan on 22/12/2016.
 */

public class PacienteAdapter extends ArrayAdapter<Paciente> {

    private Context context;
    private final ArrayList<Paciente> lista;
    //private ArrayList<Paciente> arraylist;

    public PacienteAdapter(Context context, ArrayList<Paciente> lista) {
        super(context,0,lista);
        this.context = context;
        this.lista = lista;
        //this.arraylist = arraylist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //final Paciente pac = getItem(position);
        final Paciente itemPosicao = this.lista.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.linha, parent,false);
            Log.e("Teste","e");
        }
        //final View layout = convertView;

        TextView textView = (TextView) convertView.findViewById(R.id.rowTextView);
        textView.setText(itemPosicao.getNome());


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,PacienteActivity.class);
                intent.putExtra("id",itemPosicao.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        return convertView;
    }

   /* public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        lista.clear();
        if (charText.length() == 0) {
            lista.addAll(arraylist);
        } else {
            for (Paciente wp : arraylist) {
                if (wp.getNome().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    lista.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }*/
}
