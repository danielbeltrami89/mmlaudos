package micromais.com.br.mmlaudos2.Classes;

import android.util.Log;
import java.sql.ResultSet;
import java.util.ArrayList;

import micromais.com.br.mmlaudos2.MainActivity;

public class Paciente extends _Default {

    private int id;
    public String nome;

    public ArrayList<Paciente> listaPaciente;

    public Paciente() {
        super();
        this.id = -1;
        this.nome = "";

    }

    public ArrayList<Paciente> getLista() {
        BancoConexao bd = new BancoConexao();
        listaPaciente = new ArrayList<>();
        try {
            ResultSet rs = bd.select("SELECT nome_paciente, id FROM mm_paciente WHERE cod_medico=" + MainActivity.MedCodigo + " AND situacao_envio <> 1 AND envia_medico <> 1 order by nome_paciente");
            if (rs != null) {
                while (rs.next()) {
                    Paciente paciente = new Paciente();
                    paciente.setId(rs.getInt("id"));
                    String nomePac = (rs.getString("nome_paciente"));
                    //paciente.setNome(rs.getString("nome_paciente"));
                    String novoNome = nomePac.replace("^"," ");
                    paciente.setNome(novoNome);
                    listaPaciente.add(paciente);
                    paciente = null;
                }
            }
            this._status = true;
            Log.e("Postgres", "Pesquisa: ok foi");
        } catch (Exception erro) {
            this._mensagem = erro.getMessage();
            this._status = false;
            Log.e("Postgres", "Erro pesquisa interno: " + erro);
        }

        Log.e("Postgres", "Pesquisa: Lista" + listaPaciente);
        return listaPaciente;

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + "-------" + nome;
    }


    public ArrayList<Paciente> listaPaciente() {
        return listaPaciente;
    }
}
