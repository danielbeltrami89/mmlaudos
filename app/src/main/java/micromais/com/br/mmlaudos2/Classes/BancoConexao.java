package micromais.com.br.mmlaudos2.Classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import micromais.com.br.mmlaudos2.MainActivity;

/**
 * Created by Dan on 30/12/2016.
 */

public class BancoConexao extends _Default implements Runnable {
    private Connection conn;
    private String host = MainActivity.IpServer;
    private String db = "pacsdb";
    private int port = 5432;
    private String user = "postgres";
    private String pass = "123456";
    private String url = "jdbc:postgresql://%s:%d/%s";

    public BancoConexao() {
        super();
        this.url = String.format(this.url,this.host, this.port, this.db);

        this.conecta();
        this.desconecta();
    }

    @Override
    public void run() {
        try{
            Class.forName("org.postgresql.Driver");
            this.conn = DriverManager.getConnection(this.url,this.user,this.pass);
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    private void conecta(){
        Thread thread = new Thread(this);
        thread.start();
        try{
            thread.join();
        }catch (Exception e){
            this._mensagem = e.getMessage();
            this._status = false;
        }
    }

    private void desconecta(){
        if (this.conn!= null){
            try{
                this.conn.close();
            }catch (Exception e){

            }finally {
                this.conn = null;
            }
        }
    }

    public ResultSet select(String query){
        this.conecta();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteBD(this.conn, query).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }

    public ResultSet execute(String query){
        this.conecta();
        ResultSet resultSet = null;
        try {
            resultSet = new ExecuteBD(this.conn, query).execute().get();
        }catch (Exception e){
            this._status = false;
            this._mensagem = e.getMessage();
        }
        return resultSet;
    }

}

